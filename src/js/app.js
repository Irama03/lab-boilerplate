/* eslint-disable no-undef */
// const testModules = require('./test-module');
// require('../css/app.css');
/* const leaflet = require('leaflet');
const chartjs = require('chart.js');
const lodash = require('lodash');
const dayjs = require('dayjs'); */

class User {
  constructor(gender, title, fullName, city, state, country, postcode, coordinates, timezone,
    email, bDate, age, phone, pictureLarge, pictureThumbnail) {
    this.gender = gender;
    this.title = title;
    this.full_name = fullName;
    this.city = city;
    this.state = state;
    this.country = country;
    this.postcode = postcode;
    this.coords = coordinates;
    this.timezone = timezone;
    this.email = email;
    this.b_date = bDate;
    this.age = age;
    this.phone = phone;
    this.picture_large = pictureLarge;
    this.picture_thumbnail = pictureThumbnail;
  }
}

function getGender(user) {
  if (!(_.has(user, 'gender'))) return null;
  return user.gender.charAt(0).toUpperCase() + user.gender.slice(1, user.gender.length);
}

function getTitle(user) {
  if (_.has(user, 'name')) return user.name.title;
  if (_.has(user, 'title')) return user.title;
  return null;
}

function getFullName(user) {
  if ((_.has(user, 'name'))) {
    if ((_.isNull(user.name.first)) && (_.isNull(user.name.last))) return null;
    if (_.isNull(user.name.first)) return user.name.last;
    if (_.isNull(user.name.last)) return user.name.first;
    return (`${user.name.first} ${user.name.last}`);
  }
  if ((_.has(user, 'full_name'))) return user.full_name;
  return null;
}

function getCity(user) {
  if (_.has(user, 'location')) return user.location.city;
  if (_.has(user, 'city')) return user.city;
  return null;
}

function getState(user) {
  if (_.has(user, 'location')) return user.location.state;
  if (_.has(user, 'state')) return user.state;
  return null;
}

function getCountry(user) {
  if (_.has(user, 'location')) return user.location.country;
  if (_.has(user, 'country')) return user.country;
  return null;
}

function getPostCode(user) {
  if (_.has(user, 'location')) return user.location.postcode;
  if (_.has(user, 'postcode')) return user.postcode;
  return null;
}

function getCoordinates(user) {
  if (_.has(user, 'location')) return user.location.coordinates;
  if (_.has(user, 'coordinates')) return user.coordinates;
  return null;
}

function getTimeZone(user) {
  if (_.has(user, 'location')) return user.location.timezone;
  if (_.has(user, 'timezone')) return user.timezone;
  return null;
}

function getEmail(user) {
  if (!(_.has(user, 'email'))) return null;
  return user.email;
}

function getBDate(user) {
  if (_.has(user, 'dob')) return user.dob.date;
  if (_.has(user, 'b_day')) return user.b_day;
  return null;
}

function getAge(user) {
  if (_.has(user, 'dob')) return user.dob.age;
  if (_.has(user, 'age')) return user.age;
  return null;
}

function getPhone(user) {
  if (!(_.has(user, 'phone'))) return null;
  return user.phone;
}

function getPictureLarge(user) {
  if (_.has(user, 'picture')) return user.picture.large;
  if (_.has(user, 'picture_large')) return user.picture_large;
  return null;
}

function getPictureThumbnail(user) {
  if (_.has(user, 'picture')) return user.picture.thumbnail;
  if (_.has(user, 'picture_thumbnail')) return user.picture_thumbnail;
  return null;
}

function isValidString(str) {
  return _.isString(str) && !_.isEmpty(str) && (str[0] === str[0].toUpperCase());
}

function isNumber(n) {
  if (_.isNaN(parseFloat(n))) return false;
  if (_.isNumber(n)) return true;
  return _.isFinite(parseFloat(n));
}

/*
{
  Germany: '0596-1926541',
  Ireland: '071-488-9968',
  Australia: '02-7976-3904',
  'United States': '(903)-380-2376',
  Finland: '04-421-028',
  Turkey: '(472)-638-0730',
  Switzerland: '078 301 62 63',
  'New Zealand': '(018)-588-0617',
  Spain: '987-689-092',
  Norway: '32189027',
  Denmark: '07190090',
  Iran: '015-09271245',
  Canada: '338-732-2677',
  France: '02-33-67-58-40',
  Netherlands: '(018)-382-5902'
}
 */
function isValidPhone(phone, country) {
  if (country === 'Germany') return (new RegExp(/([0-9]{4}[-][0-9]{7})/)).test(phone);
  if (country === 'Ireland' || country === 'Canada') return (new RegExp(/([0-9]{3}[-][0-9]{3}[-][0-9]{4})/)).test(phone);
  if (country === 'Australia') return (new RegExp(/([0-9]{2}[-][0-9]{4}[-][0-9]{4})/)).test(phone);
  if (country === 'United States' || country === 'Turkay' || country === 'New Zealand' || country === 'Netherlands') return (new RegExp(/([(][0-9]{3}[)][-][0-9]{3}[-][0-9]{4})/)).test(phone);
  if (country === 'Finland') return (new RegExp(/([0-9]{2}[-][0-9]{3}[-][0-9]{3})/)).test(phone);
  if (country === 'Switzerland') return (new RegExp(/([0-9]{3}[\s][0-9]{3}[\s][0-9]{2}[\s][0-9]{2})/)).test(phone);
  if (country === 'Spain') return (new RegExp(/([0-9]{3}[-][0-9]{3}[-][0-9]{3})/)).test(phone);
  if (country === 'Norway' || country === 'Denmark') return (new RegExp(/([0-9]{8})/)).test(phone);
  if (country === 'Iran') return (new RegExp(/([0-9]{3}[-][0-9]{8})/)).test(phone);
  if (country === 'France') return (new RegExp(/([0-9]{2}[-][0-9]{2}[-][0-9]{2}[-][0-9]{2}[-][0-9]{2})/)).test(phone);
  return (new RegExp(/([(]?[0-9]+[)]?[-]?[0-9]+[-]?[0-9]+([-][0-9]+)?)/)).test(phone);
}

function isValidEmail(email) {
  const regEmail = new RegExp(/([\w-.]+@([\w-]+.)+[\w-]{2,4})/);
  return regEmail.test(email);
}

// without state, because there isn't such field in form
function userIsValid(user) {
  return isValidString(user.full_name) && isValidString(user.gender) && isValidString(user.note)
    && isValidString(user.city) && isValidString(user.country)
    && isNumber(user.age) && isValidPhone(user.phone, user.country) && isValidEmail(user.email);
}

function contains(arr, user) {
  const id0 = user.id;
  // eslint-disable-next-line no-param-reassign
  user.id = 0;
  for (let i = 0; i < arr.length; i += 1) {
    const id1 = arr[i].id;
    // eslint-disable-next-line no-param-reassign
    arr[i].id = 0;
    if (JSON.stringify(arr[i]) === JSON.stringify(user)) {
      // eslint-disable-next-line no-param-reassign
      user.id = id0;
      // eslint-disable-next-line no-param-reassign
      arr[i].id = id1;
      return true;
    }
    // eslint-disable-next-line no-param-reassign
    arr[i].id = id1;
  }
  // eslint-disable-next-line no-param-reassign
  user.id = id0;
  return false;
}

const countriesSt = {
  Australia: 0,
  Brazil: 0,
  Canada: 0,
  Denmark: 0,
  Finland: 0,
  France: 0,
  Germany: 0,
  Iran: 0,
  Ireland: 0,
  Netherlands: 0,
  'New Zealand': 0,
  Norway: 0,
  Spain: 0,
  Turkey: 0,
  Switzerland: 0,
  Ukraine: 0,
  'United Kingdom': 0,
  'United States': 0,
  Other: 0,
};

const gendersSt = {
  Male: 0,
  Female: 0,
};

const agesSt = {
  16: 0,
  26: 0,
  36: 0,
  46: 0,
  56: 0,
  66: 0,
  76: 0,
  86: 0,
};

function increaseQuantOfParamsSt(country, gender, age) {
  if (_.has(countriesSt, country)) {
    countriesSt[country] += 1;
  } else countriesSt.Other += 1;
  gendersSt[gender] += 1;
  _.keysIn(agesSt).forEach((key) => {
    if (age >= parseFloat(key) && age < (parseFloat(key) + 10)) {
      agesSt[key] += 1;
    }
  });
}

let counter = 0;

function formatUsers(arr) {
  const result = [];
  arr.forEach((user) => {
    if (!(_.has(user, 'bg_color'))) {
      const newUser = new User(getGender(user), getTitle(user), getFullName(user), getCity(user),
        getState(user), getCountry(user), getPostCode(user), getCoordinates(user),
        getTimeZone(user), getEmail(user), getBDate(user), getAge(user), getPhone(user),
        getPictureLarge(user), getPictureThumbnail(user));
      newUser.id = counter;
      counter += 1;
      newUser.favorite = false;
      newUser.course = 'English';
      newUser.bg_color = '#78a4d9';
      newUser.note = 'Some note';
      const newUser0 = {};
      _.forIn(newUser, (value, key) => {
        newUser0[key] = newUser[key];
      });
      if (userIsValid(newUser0) && !(contains(result, newUser0))) {
        result.push(newUser0);
        increaseQuantOfParamsSt(newUser0.country, newUser0.gender, newUser0.age);
      }
    } else if (!(contains(result, user))) {
      result.push(user);
      increaseQuantOfParamsSt(user.country, user.gender, user.age);
    }
  });
  return result;
}

/*
comparator for age:
<0 - smaller
==0 - equal
>0 - bigger
*/
function filterUsers(arr, countries = [], favorites = [], genders = [], age = '', comparator = 0) {
  return _.filter(arr, (user) => {
    let suitable = true;
    if (_.isEmpty(countries) || countries.length === 245) suitable = true;
    else if (!countries.includes(user.country)) return false;
    if (_.isEmpty(favorites) || favorites.length === 2) suitable = true;
    else if ((favorites[0]) !== user.favorite) return false;
    if (_.isEmpty(genders) || genders.length === 2) suitable = true;
    else if (!genders.includes(user.gender)) return false;
    if (!_.isEmpty(age)) {
      if (comparator === 0 && (user.age !== parseFloat(age))) suitable = false;
      else if (comparator < 0 && (user.age >= parseFloat(age))) suitable = false;
      else if (comparator > 0 && (user.age <= parseFloat(age))) suitable = false;
    }
    return suitable;
  });
}

let sortParam = '';

function compareSortParam(a0, b0) {
  const a = (_.isNull(a0[sortParam])) ? '' : a0[sortParam].toLowerCase();
  const b = (_.isNull(b0[sortParam])) ? '' : b0[sortParam].toLowerCase();
  if (a < b) return -1;
  if (a > b) return 1;
  return 0;
}

function compareSortParamReverse(a0, b0) {
  const a = (_.isNull(a0[sortParam])) ? '' : a0[sortParam].toLowerCase();
  const b = (_.isNull(b0[sortParam])) ? '' : b0[sortParam].toLowerCase();
  if (a > b) return -1;
  if (a < b) return 1;
  return 0;
}

/* function compareAge(a0, b0) {
  return (a0.age - b0.age);
} */

function compareAgeReverse(a0, b0) {
  return (b0.age - a0.age);
}

function compareBDate(a0, b0) {
  return (new Date(a0.b_date) - new Date(b0.b_date));
}

function compareBDateReverse(a0, b0) {
  return (new Date(b0.b_date) - new Date(a0.b_date));
}

function sortUsers(arr, param, order) {
  if (param === 'age') {
    if (order) return _.sortBy(arr, 'age');
    return arr.sort(compareAgeReverse);
  }
  if (param === 'b_date') {
    if (order) return arr.sort(compareBDate);
    return arr.sort(compareBDateReverse);
  }
  sortParam = param;
  if (order) return arr.sort(compareSortParam);
  return arr.sort(compareSortParamReverse);
}

function findUsersOneParam(arr, param, val) {
  if (_.isNull(arr)) return null;
  if (_.isEmpty(val)) return arr;
  if (param === 'full_name' || param === 'note') return _.filter(arr, (user) => (!_.isNull(user[param]) && (user[param].toLowerCase()).includes(val.toLowerCase())));
  if (param === 'age') return _.filter(arr, (user) => user[param] === parseFloat(val));
  return null;
}

function findIsEmpty(valName, valNote, valAge) {
  return _.isEmpty(valName) && _.isEmpty(valNote) && _.isEmpty(valAge);
}

function findUsers(arr, valName, valNote, valAge) {
  if (findIsEmpty(valName, valNote, valAge)) return arr;
  return findUsersOneParam(findUsersOneParam(findUsersOneParam(arr, 'full_name', valName), 'note', valNote), 'age', valAge);
}

const deepCopyFunction = (inObject) => {
  let value;
  let key;
  if (typeof inObject !== 'object' || _.isNull(inObject)) {
    return inObject;
  }
  const outObject = Array.isArray(inObject) ? [] : {};
  // eslint-disable-next-line guard-for-in,no-restricted-syntax
  for (key in inObject) {
    value = inObject[key];
    outObject[key] = deepCopyFunction(value);
  }
  return outObject;
};

let initialUsers;
let users;
let currentUser;
let compactCards;
let rowsOfTable;

function catchErrors(response) {
  if (!response.ok) throw new Error(`Network Error: ${response.status}`);
  return response;
}

function getValOfElemById(id) {
  return document.getElementById(id).value;
}

function getGenderSymbol(gender) {
  if (gender === 'Female') return 'F';
  if (gender === 'Male') return 'M';
  return 'O';
}

const latitude = 50.4501;
const longitude = 30.5234;
const map = L.map('map').setView([latitude, longitude], 12);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiaXJhbWEwMyIsImEiOiJja3Z2YXA1OXcwMWNkMm9uNG9yYTZ3OWswIn0.2mqpRgtLe0Q-Yak5Crq9Hw', {
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
  maxZoom: 20,
  id: 'mapbox/streets-v11',
  tileSize: 512,
  zoomOffset: -1,
}).addTo(map);
const marker = new L.Marker([latitude, longitude]);
marker.addTo(map);

function changeMap(lat = latitude, lon = longitude) {
  const latlng = new L.LatLng(lat, lon);
  map.setView(latlng, 12);
  marker.setLatLng(latlng);
}

const countryCanvas = document.getElementById('country-chart').getContext('2d');
const genderCanvas = document.getElementById('gender-chart').getContext('2d');
const ageCanvas = document.getElementById('age-chart').getContext('2d');

const countryData = {
  labels: _.keysIn(countriesSt),
  datasets: [
    {
      data: [],
      backgroundColor: [
        '#ff0000',
        '#ff4900',
        '#ff7c00',
        '#ffa900',
        '#ffc600',
        '#ffe200',
        '#baf300',
        '#62e200',
        '#00bb3f',
        '#00af64',
        '#028e9b',
        '#104ba9',
        '#301680',
        '#680bab',
        '#8c04a8',
        '#b90091',
        '#cd0074',
        '#de0052',
        '#ec0033',
      ],
    }],
};

const genderData = {
  labels: _.keysIn(gendersSt),
  datasets: [
    {
      data: [],
      backgroundColor: [
        '#104ba9',
        '#de0052',
      ],
    }],
};

const ageData = {
  labels: [
    '16-25',
    '26-35',
    '36-45',
    '46-55',
    '56-65',
    '66-75',
    '76-85',
    '86-95',
  ],
  datasets: [
    {
      data: [],
      backgroundColor: [
        '#ff0000',
        '#ffa900',
        '#ffe200',
        '#62e200',
        '#028e9b',
        '#301680',
        '#b90091',
        '#de0052',
      ],
    }],
};

const options = {
  responsive: true,
  maintainAspectRatio: true,
};

let countryChart;
let genderChart;
let ageChart;

function getGraduses(quant) {
  return (180 * quant) / (initialUsers.length);
}

function createOrUpdateCharts(firstly = false) {
  if (!firstly) {
    countryChart.destroy();
    genderChart.destroy();
    ageChart.destroy();
    countryData.datasets[0].data = [];
    genderData.datasets[0].data = [];
    ageData.datasets[0].data = [];
  }
  _.keysIn(countriesSt).forEach((key) => {
    countryData.datasets[0].data.push(getGraduses(countriesSt[key]));
  });
  _.keysIn(gendersSt).forEach((key) => {
    genderData.datasets[0].data.push(getGraduses(gendersSt[key]));
  });
  _.keysIn(agesSt).forEach((key) => {
    ageData.datasets[0].data.push(getGraduses(agesSt[key]));
  });
  countryChart = new Chart(countryCanvas, {
    type: 'pie',
    data: countryData,
  }, options);
  genderChart = new Chart(genderCanvas, {
    type: 'pie',
    data: genderData,
  }, options);
  ageChart = new Chart(ageCanvas, {
    type: 'pie',
    data: ageData,
  }, options);
}

function getQuantOfDaysToNextBirthday(dOfB) {
  const dateOfBirth = dayjs(dOfB);
  // alert('dateOfBirth: ' + dateOfBirth.format('DD.MM.YYYY'));
  if (!dateOfBirth.isValid()) return 'null';
  let currentDate = dayjs();
  // eslint-disable-next-line max-len
  currentDate = dayjs(new Date(currentDate.year(), currentDate.month(), currentDate.date(), 11, 0, 0));
  const currentYear = currentDate.year();
  // eslint-disable-next-line max-len
  let nextBirthday = dayjs(new Date(currentYear, dateOfBirth.month(), dateOfBirth.date(), 11, 0, 0));
  if (nextBirthday.isBefore(currentDate) || nextBirthday.isSame(currentDate)) {
    // eslint-disable-next-line max-len
    nextBirthday = dayjs(new Date(currentYear + 1, dateOfBirth.month(), dateOfBirth.date(), 11, 0, 0));
  }
  return nextBirthday.diff(currentDate, 'day');
}

function addListenerForShowingCardOfTeacher(img, user) {
  img.addEventListener('click', () => {
    document.getElementById('map').style.display = 'none';
    currentUser = user;
    const star = document.getElementById('teacher-info-star-img');
    if (user.favorite === true) star.setAttribute('src', './images/star.png');
    else star.setAttribute('src', './images/starContour.jpg');
    if (user.picture_large !== null) document.getElementsByClassName('teachers-info-img')[0].setAttribute('src', user.picture_large);
    else document.getElementsByClassName('teachers-info-img')[0].setAttribute('src', './images/default-user-image.png');
    document.getElementsByClassName('teacher-info-name')[0].innerText = user.full_name;
    if (currentUser.coords !== null) {
      changeMap(currentUser.coords.latitude, currentUser.coords.longitude);
    } else changeMap();
    document.getElementsByClassName('teacher-info-city-country')[0].innerText = `${user.city}, ${user.country}`;
    document.getElementsByClassName('teacher-info-age-gender')[0].innerText = `${user.age} (${getQuantOfDaysToNextBirthday(user.b_date)} days to next birthday), ${getGenderSymbol(user.gender)}`;
    document.getElementsByClassName('teacher-info-email')[0].innerText = user.email;
    document.getElementsByClassName('teacher-info-tel')[0].innerText = user.phone;
    document.getElementsByClassName('teacher-info-notes')[0].innerText = user.note;
    document.getElementById('wr2').className = 'showingPopup';
    document.getElementsByClassName('popup-card-of-teacher')[0].style.top = '5px';
  });
}

document.getElementById('teacher-info-toggle-map').addEventListener('click', () => {
  const aMap = document.getElementById('map');
  let { display } = aMap.style;
  if (display === 'none') display = 'grid';
  else display = 'none';
  aMap.style.display = display;
  map.invalidateSize();
});

function addStar(div, id) {
  if (div !== null) {
    // eslint-disable-next-line no-param-reassign
    div.innerHTML = `<span class="star-img">&#9733;</span>${div.innerHTML}`;
    // eslint-disable-next-line no-param-reassign
    div.children[0].id = `star-img-${id}`;
  }
}

let nextNum = 1;

function addCompactCard(user, parentDiv, forFavorite) {
  let item;
  let img;
  const name = user.full_name.split(' ')[0];
  const surname = user.full_name.split(' ')[1];
  if (user.picture_large !== null) {
    img = document.createElement('img');
    img.setAttribute('src', user.picture_large);
    img.setAttribute('alt', 'photo');
  } else {
    img = document.createElement('div');
    img.innerText = `${name[0]}.${surname[0]}`;
  }
  const divName = document.createElement('div');
  const divCountry = document.createElement('div');
  divName.className = 'teachers__item-name';
  divCountry.className = 'teachers__item-country';
  divName.innerHTML = `${name}<br/>${surname}`;
  divCountry.innerText = user.country;
  if (!forFavorite) {
    item = document.createElement('div');
    item.className = 'teachers__content-item';
    item.id = `teachers__content-item-${user.id}`;
    img.className = 'teachers__item-img';
    item.style.display = 'none';
    item.setAttribute('data-num', nextNum);
    const divTwoPictures = document.createElement('div');
    divTwoPictures.className = 'teachers__item-two-pictures';
    divTwoPictures.id = `teachers__item-two-pictures-${user.id}`;
    if (user.favorite) addStar(divTwoPictures, user.id);
    divTwoPictures.appendChild(img);
    item.appendChild(divTwoPictures);
    item.appendChild(divName);
    item.appendChild(divCountry);
    addListenerForShowingCardOfTeacher(divTwoPictures, user);
    compactCards.push(item);
  } else {
    item = document.createElement('li');
    item.className = 'teachers__favorites-content-item-item';
    item.id = `teachers__favorites-content-item-${user.id}`;
    const divItem = document.createElement('div');
    divItem.className = 'teachers__favorites-content-item';
    divItem.id = 'teachers__favorites-content-item';
    img.id = 'teachers__item-img';
    img.className = 'teachers__favorites-item-img';
    divItem.appendChild(img);
    divItem.appendChild(divName);
    divItem.appendChild(divCountry);
    item.appendChild(divItem);
    addListenerForShowingCardOfTeacher(img, user);
  }
  document.getElementById(parentDiv).appendChild(item);
}

function addRowOfTable(user) {
  const tr = document.createElement('tr');
  tr.className = 'section-statistics__table-tr';
  tr.setAttribute('data-num', nextNum);
  nextNum += 1;
  tr.id = `section-statistics__table-tr-${user.id}`;
  const tdName = document.createElement('td');
  const tdAge = document.createElement('td');
  const tdBDate = document.createElement('td');
  const tdGender = document.createElement('td');
  const tdCountry = document.createElement('td');
  tdName.innerText = user.full_name;
  tdAge.innerText = user.age;
  const date = dayjs(user.b_date);
  tdBDate.innerText = date.format('DD.MM.YYYY');
  tdGender.innerText = user.gender;
  tdCountry.innerText = user.country;
  tr.appendChild(tdName);
  tr.appendChild(tdAge);
  tr.appendChild(tdBDate);
  tr.appendChild(tdGender);
  tr.appendChild(tdCountry);
  rowsOfTable.push(tr);
  document.getElementById('body-of-teachers-table').appendChild(tr);
}

const initialQueryString = 'https://randomuser.me/api?';
let queryString = initialQueryString;

const count = 50;
const cnt = 10;
let cntPage = Math.ceil(count / cnt);
let mainPage;

function addOrUpdatePages() {
  const paginator = document.querySelector('.paginator');
  let page = '';
  for (let i = 0; i < cntPage; i += 1) {
    page += `<span data-page=${i * cnt} id=page${i + 1}>${i + 1}</span>`;
  }
  paginator.innerHTML = page;
  mainPage = document.getElementById('page1');
  mainPage.classList.add('paginator_active');
}

addOrUpdatePages();

function addOnePage() {
  cntPage += 1;
  document.getElementById('paginator').innerHTML += `<span data-page=${(cntPage - 1) * cnt} id=page${cntPage}>${cntPage}</span>`;
}

let compCards;
let rowsTable;

function addUserGraphically(user, byPopup = false) {
  addCompactCard(user, 'section-teachers__content', false);
  addRowOfTable(user);
  if (user.favorite) addCompactCard(user, 'section-favorites__content', true);
  if (byPopup) {
    if ((nextNum - (cntPage * cnt)) === 2) addOnePage();
    compCards = document.querySelectorAll('.teachers__content-item');
    rowsTable = document.querySelectorAll('.section-statistics__table-tr');
    document.getElementById(`page${cntPage}`).click();
  }
}

function loadUsers(users0, firstly = true) {
  users0.forEach((user) => {
    addUserGraphically(user);
  });
  if (firstly) {
    const divSpace = document.createElement('div');
    divSpace.className = 'space';
    document.getElementById('carousel').appendChild(divSpace);
  }
}

function updateUsers(users0, firstly = false) {
  const parentDiv0 = document.getElementById('section-teachers__content');
  parentDiv0.innerHTML = '';
  const parentDiv1 = document.getElementById('body-of-teachers-table');
  parentDiv1.innerHTML = '';
  if (!firstly) nextNum = 1;
  users0.forEach((user) => {
    compactCards.forEach((compactCard) => {
      if (compactCard.id === `teachers__content-item-${user.id}`) {
        if (!firstly) compactCard.setAttribute('data-num', nextNum);
        parentDiv0.appendChild(compactCard);
      }
    });
    rowsOfTable.forEach((row) => {
      if (row.id === `section-statistics__table-tr-${user.id}`) {
        if (!firstly) {
          row.setAttribute('data-num', nextNum);
          nextNum += 1;
        }
        parentDiv1.appendChild(row);
      }
    });
  });
  if (!firstly) {
    compCards = document.querySelectorAll('.teachers__content-item');
    rowsTable = document.querySelectorAll('.section-statistics__table-tr');
    cntPage = Math.ceil(users0.length / cnt);
    addOrUpdatePages();
    document.getElementById('page1').click();
  }
}

function show10Users() {
  compCards = document.querySelectorAll('.teachers__content-item');
  rowsTable = document.querySelectorAll('.section-statistics__table-tr');
  for (let i = 0; i < compCards.length; i += 1) {
    if (i < cnt) {
      compCards[i].style.display = 'block';
      rowsTable[i].style.display = 'table-row';
    }
  }
}

function httpGet(url) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    // eslint-disable-next-line func-names
    xhr.onload = function () {
      if (this.status === 200) {
        resolve(this.response);
      } else {
        const error = new Error(this.statusText);
        error.code = this.status;
        reject(error);
      }
    };
    // eslint-disable-next-line func-names
    xhr.onerror = function () {
      reject(new Error('Network Error'));
    };
    xhr.send();
  });
}

function setVarsInit() {
  currentUser = null;
  compactCards = [];
  rowsOfTable = [];
}

function get50Users(firstly = true) {
  setVarsInit();
  httpGet(`${queryString}results=50`).then((response) => {
    const obj = (JSON.parse(response));
    initialUsers = formatUsers(obj.results);
    users = deepCopyFunction(initialUsers);
    loadUsers(users, firstly);
    updateUsers(users, true);
    show10Users();
    createOrUpdateCharts(true);
  });
}

function getNext10Users(firstly = false) {
  setVarsInit();
  document.getElementById('section-favorites__content').innerHTML = '';
  httpGet(`${queryString}&results=10`).then((response) => {
    const obj = (JSON.parse(response));
    _.keysIn(countriesSt).forEach((key) => {
      countriesSt[key] = 0;
    });
    _.keysIn(gendersSt).forEach((key) => {
      gendersSt[key] = 0;
    });
    _.keysIn(agesSt).forEach((key) => {
      agesSt[key] = 0;
    });
    if (!firstly) initialUsers = formatUsers(initialUsers.concat(obj.results));
    else initialUsers = formatUsers(obj.results);
    users = deepCopyFunction(initialUsers);
    nextNum = 1;
    loadUsers(users);
    updateUsers(users, !firstly);
    if (firstly) {
      cntPage = 1;
      addOrUpdatePages();
      show10Users();
    } else {
      addOnePage();
      compCards = document.querySelectorAll('.teachers__content-item');
      rowsTable = document.querySelectorAll('.section-statistics__table-tr');
      document.getElementById(`page${cntPage}`).click();
    }
    createOrUpdateCharts();
  });
}

get50Users();

document.getElementById('teacher-info-star-img').addEventListener('click', () => {
  const star = document.getElementById('teacher-info-star-img');
  if (star.getAttribute('src') === './images/star.png') {
    star.setAttribute('src', './images/starContour.jpg');
    currentUser.favorite = false;
    const notFavUser = document.getElementById(`teachers__favorites-content-item-${currentUser.id}`);
    if (notFavUser.parentNode) notFavUser.parentNode.removeChild(notFavUser);
    const userStar = document.getElementById(`star-img-${currentUser.id}`);
    if (userStar.parentNode) userStar.parentNode.removeChild(userStar);
  } else {
    star.setAttribute('src', './images/star.png');
    currentUser.favorite = true;
    addCompactCard(currentUser, 'section-favorites__content', true);
    addStar(document.getElementById(`teachers__item-two-pictures-${currentUser.id}`), currentUser.id);
  }
  users.forEach((user) => {
    if (user.id === currentUser.id) {
      // eslint-disable-next-line no-param-reassign
      user.favorite = currentUser.favorite;
    }
  });
  initialUsers.forEach((user) => {
    if (user.id === currentUser.id) {
      // eslint-disable-next-line no-param-reassign
      user.favorite = currentUser.favorite;
    }
  });
});

document.getElementById('close-card-of-teacher').addEventListener('click', () => {
  document.getElementById('wr2').className = 'wr2';
  document.getElementsByClassName('popup-card-of-teacher')[0].style.top = '-5000px';
});

document.getElementById('close-add-teacher').addEventListener('click', () => {
  document.getElementById('wr1').className = 'wr1';
  document.getElementsByClassName('popup-add-teacher')[0].style.top = '-1400px';
});

const initialStateOfUsers = {
  filterApplied: false,
  filterShowOnlyFavorites: [],
  filterGenders: [],
  filterAge: 16,
  filterComparator: 1,
  filterCountries: [],
  findApplied: false,
  findName: '',
  findNote: '',
  findAge: '',
  sortApplied: false,
  sortParam: 'none',
  sortOrder: false,
};
let stateOfUsers = deepCopyFunction(initialStateOfUsers);

function setCheckboxesNotChosen(name) {
  const checkboxes = document.getElementsByName(name);
  for (let i = 0; i < checkboxes.length; i += 1) {
    checkboxes[i].checked = false;
  }
}

function applyDefaultFilters() {
  stateOfUsers = deepCopyFunction(initialStateOfUsers);
  document.getElementById('search-name').value = '';
  document.getElementById('search-note').value = '';
  document.getElementById('search-age').value = '';
  setCheckboxesNotChosen('countries');
  document.getElementById('filter-favorites').checked = false;
  setCheckboxesNotChosen('sexes');
  document.getElementsByName('filter-show-age')[1].checked = true;
  document.getElementById('filter-age').value = initialStateOfUsers.filterAge;
}

function getValuesOfChosenCheckboxes(name) {
  const checkboxes = document.getElementsByName(name);
  const res = [];
  for (let i = 0; i < checkboxes.length; i += 1) {
    if (checkboxes[i].checked) {
      res.push(checkboxes[i].getAttribute('value'));
    }
  }
  return res;
}

function getFilteredCountries() {
  return getValuesOfChosenCheckboxes('countries');
}

function getFilteredFavorites() {
  if (document.getElementById('filter-favorites').checked) {
    return [true];
  }
  return [true, false];
}

function getFilteredGenders() {
  return getValuesOfChosenCheckboxes('sexes');
}

function getComparator() {
  const rad = document.getElementsByName('filter-show-age');
  if (rad[0].checked) return -1;
  if (rad[1].checked) return 1;
  return 0;
}

function doFilter(users0 = initialUsers, fromButton = false) {
  if (fromButton) {
    stateOfUsers.filterCountries = getFilteredCountries();
    stateOfUsers.filterShowOnlyFavorites = getFilteredFavorites();
    stateOfUsers.filterGenders = getFilteredGenders();
    stateOfUsers.filterAge = (document.getElementById('filter-age').value);
    stateOfUsers.filterComparator = getComparator();
  }
  return filterUsers(users0, stateOfUsers.filterCountries, stateOfUsers.filterShowOnlyFavorites,
    stateOfUsers.filterGenders, stateOfUsers.filterAge, stateOfUsers.filterComparator);
}

function doFind(users0 = initialUsers, fromButton = false) {
  if (fromButton) {
    stateOfUsers.findName = document.getElementById('search-name').value;
    stateOfUsers.findNote = document.getElementById('search-note').value;
    stateOfUsers.findAge = document.getElementById('search-age').value;
  }
  return findUsers(users0, stateOfUsers.findName, stateOfUsers.findNote, stateOfUsers.findAge);
}

function doSort(param) {
  let flag = false;
  if (stateOfUsers.findApplied) {
    users = doFind();
    flag = true;
  }
  if (stateOfUsers.filterApplied) {
    if (flag) users = doFilter(users);
    else {
      users = doFilter();
      flag = true;
    }
  }
  stateOfUsers.sortOrder = !stateOfUsers.sortOrder;
  if (flag) users = sortUsers(users, param, stateOfUsers.sortOrder);
  else users = sortUsers(initialUsers, param, stateOfUsers.sortOrder);
  stateOfUsers.sortApplied = true;
  stateOfUsers.sortParam = param;
  updateUsers(users);
}

document.getElementById('apply-filter').addEventListener('click', () => {
  let flag = false;
  if (stateOfUsers.findApplied) {
    users = doFind();
    flag = true;
  }
  if (stateOfUsers.sortApplied) {
    if (flag) users = sortUsers(users, stateOfUsers.sortParam, stateOfUsers.sortOrder);
    else {
      users = sortUsers(initialUsers, stateOfUsers.sortParam, stateOfUsers.sortOrder);
      flag = true;
    }
  }
  if (flag) users = doFilter(users, true);
  else users = doFilter(initialUsers, true);
  stateOfUsers.filterApplied = true;
  updateUsers(users);
});

document.getElementById('section-statistics__table-name-of-col-name').addEventListener('click', () => {
  doSort('full_name');
});

document.getElementById('section-statistics__table-name-of-col-age').addEventListener('click', () => {
  doSort('age');
});

document.getElementById('section-statistics__table-name-of-col-b_date').addEventListener('click', () => {
  doSort('b_date');
});

document.getElementById('section-statistics__table-name-of-col-gender').addEventListener('click', () => {
  doSort('gender');
});

document.getElementById('section-statistics__table-name-of-col-country').addEventListener('click', () => {
  doSort('country');
});

document.getElementById('search-button').addEventListener('click', () => {
  let flag = false;
  if (stateOfUsers.filterApplied) {
    users = doFilter();
    flag = true;
  }
  if (stateOfUsers.sortApplied) {
    if (flag) users = sortUsers(users, stateOfUsers.sortParam, stateOfUsers.sortOrder);
    else {
      users = sortUsers(initialUsers, stateOfUsers.sortParam, stateOfUsers.sortOrder);
      flag = true;
    }
  }
  if (flag) users = doFind(users, true);
  else users = doFind(initialUsers, true);
  stateOfUsers.findApplied = !findIsEmpty(document.getElementById('search-name').value, document.getElementById('search-note').value, document.getElementById('search-age').value);
  updateUsers(users);
});

function getGenderFromForm(name) {
  const rad = document.getElementsByName(name);
  if (rad[0].checked) {
    if (name === 'genderF') return 'Female';
    return 'female';
  }
  if (name === 'genderF') return 'Male';
  return 'male';
}

function createUser() {
  const newUser = new User(getGenderFromForm('genderF'), null, getValOfElemById('name'),
    getValOfElemById('city'), null, getValOfElemById('country'), null, null,
    null, getValOfElemById('email'), null, parseFloat(getValOfElemById('age')), getValOfElemById('phone'),
    null, null);
  newUser.id = counter;
  counter += 1;
  newUser.favorite = false;
  newUser.course = 'English';
  newUser.bg_color = getValOfElemById('color');
  newUser.note = 'Some note';
  const newUser0 = {};
  _.keysIn(newUser).forEach((key) => {
    newUser0[key] = newUser[key];
  });
  if (!userIsValid(newUser0)) {
    // eslint-disable-next-line no-alert
    alert('Invalid data of teacher!');
    return null;
  }
  if (contains(initialUsers, newUser0)) {
    // eslint-disable-next-line no-alert
    alert('This teacher already exists!');
    return null;
  }
  initialUsers.push(newUser0);
  users.push(newUser0);
  increaseQuantOfParamsSt(newUser0.country, newUser0.gender, newUser0.age);
  createOrUpdateCharts();
  return newUser0;
}

document.getElementById('btn-add-teacher').addEventListener('click', () => {
  const user = createUser();
  if (user !== null) {
    fetch('http://localhost:8081/users', {
      method: 'POST',
      body: JSON.stringify(user),
      headers: { 'Content-type': 'application/json' },
    }).then(catchErrors);
    document.getElementById('wr1').className = 'wr1';
    document.getElementsByClassName('popup-add-teacher')[0].style.top = '-1400px';
    // eslint-disable-next-line no-alert
    alert('Teacher added successfully!');
    applyDefaultFilters();
    addUserGraphically(user, true);
  }
});

document.getElementById('add-teacher-button-1').addEventListener('click', () => {
  document.getElementById('wr1').className = 'showingPopup';
  document.getElementsByClassName('popup-add-teacher')[0].style.top = '0';
});

document.getElementById('add-teacher-button-2').addEventListener('click', () => {
  document.getElementById('wr1').className = 'showingPopup';
  document.getElementsByClassName('popup-add-teacher')[0].style.top = '0';
});

function countryToNationality(country) {
  switch (country) {
    case 'Australia':
      return 'au';
    case 'Brazil':
      return 'br';
    case 'Canada':
      return 'ca';
    case 'Denmark':
      return 'dk';
    case 'Finland':
      return 'fi';
    case 'France':
      return 'fr';
    case 'Germany':
      return 'de';
    case 'Iran':
      return 'ir';
    case 'Ireland':
      return 'ie';
    case 'Netherlands':
      return 'nl';
    case 'New Zealand':
      return 'nz';
    case 'Norway':
      return 'no';
    case 'Spain':
      return 'es';
    case 'Turkey':
      return 'tr';
    case 'Switzerland':
      return 'ch';
    case 'United Kingdom':
      return 'gb';
    case 'United States':
      return 'us';
    default:
      return null;
  }
}

function encodeQueryData(queryParams) {
  const ret = [];
  // eslint-disable-next-line guard-for-in,no-restricted-syntax
  for (const p in queryParams) {
    if (queryParams[p] !== null) {
      ret.push(`${encodeURIComponent(p)}=${encodeURIComponent(queryParams[p])}`);
    }
  }
  return ret.join('&');
}

const queryParams = { gender: null, nat: null };

document.getElementById('get-users').addEventListener('click', () => {
  queryParams.gender = (document.getElementById('get-gender').checked) ? getGenderFromForm('getGender') : null;
  if (document.getElementById('get-country').checked) {
    const nationality = countryToNationality(getValOfElemById('get-user-country'));
    // eslint-disable-next-line no-alert
    if (_.isNull(nationality)) {
      alert('You can not get new teachers from chosen country!');
      queryParams.nat = null;
    } else queryParams.nat = nationality;
  } else queryParams.nat = null;
  queryString = `${initialQueryString}${encodeQueryData(queryParams)}`;
  applyDefaultFilters();
  getNext10Users(true);
});

document.getElementById('next-10-button').addEventListener('click', () => {
  applyDefaultFilters();
  getNext10Users();
});

document.getElementById('paginator').addEventListener('click', () => {
  // eslint-disable-next-line no-restricted-globals
  const e = event || window.event;
  const target0 = e.target;
  const id0 = target0.id;

  if (target0.tagName.toLowerCase() !== 'span') return;
  const dataPage = +target0.dataset.page;
  document.getElementById(mainPage.id).className = '';
  mainPage = document.getElementById(id0);
  mainPage.classList.add('paginator_active');

  let j = 0;
  for (let i = 0; i < compCards.length; i += 1) {
    const dataNum = compCards[i].dataset.num;
    if (dataNum <= dataPage || dataNum >= dataPage) {
      compCards[i].style.display = 'none';
      rowsTable[i].style.display = 'none';
    }
  }

  for (let i = dataPage; i < compCards.length; i += 1) {
    if (j >= cnt) break;
    compCards[i].style.display = 'block';
    rowsTable[i].style.display = 'table-row';
    j += 1;
  }
});
